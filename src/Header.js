import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';

export default class Header extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('./assets/logo.jpg')} resizeMode={'cover'} style={styles.logo}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  logo: {
    width: 150,
    height: 26
  }
});
