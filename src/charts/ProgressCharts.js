import React, {Component} from 'react';
import { View, Text, Dimensions } from 'react-native';
import { ProgressChart } from 'react-native-chart-kit'
import { Svg, Circle, Line, Rect, Path, } from 'react-native-svg';

export default class ProgressCharts extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: [0,0,0]
    }

  }

  render() {
    const width = (Dimensions.get('window').width - 10 )
    return (
      <ProgressChart
      data={this.props.data}
        width={width}
        height={this.props.height}
        chartConfig={{
          backgroundColor: this.props.backgroundColor || '#ffffff',
          backgroundGradientFrom: this.props.backgroundGradientFrom || '#ffffff',
          backgroundGradientTo: this.props.backgroundGradientTo || '#ffffff',
          decimalPlaces: 2, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(${this.props.color}, ${opacity})`,
          style: {
            borderRadius: 0
            //transition: '.25s all'
            //transition-delay: '1s'
          }
        }}
      />
    );
  }
}
