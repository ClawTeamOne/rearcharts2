import React, {Component} from 'react';
import { View, Text } from 'react-native';
import { LineChart } from 'react-native-chart-kit'
import { Svg, Circle, Line, Rect, Path, } from 'react-native-svg';

//import { SharedElement } from 'react-native-motion';

export default class LineCharts extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: [0,0,0,0,0,0]
    }

    this.setPoint(2)
    this.setPoint(1)
    this.setPoint(3)
    this.setPoint(4)
    this.setPoint(0)
    this.setPoint(5)

  }

  setPoint = (n) => {
    this.sleep(0).then(() => {

    let newVal = []

      newVal = [...this.state.data]
      newVal[n] = this.props.data[n]
      this.setState({
        data: newVal
      });

  });
  }

  sleep = (time) => {
  return new Promise((resolve) => setTimeout(resolve, time));
}

  render() {
    return (
      //<SharedElement id="source">
      <View>
        <LineChart
          data={{
            labels: this.props.labels,
            datasets: [{
              data: this.state.data
            }]
          }}
          width={this.props.width} // from react-native
          height={this.props.height}
          yAxisLabel={'$'}
          chartConfig={{
            backgroundColor: this.props.backgroundColor || '#ffffff',
            backgroundGradientFrom: this.props.backgroundGradientFrom || '#ffffff',
            backgroundGradientTo: this.props.backgroundGradientTo || '#ffffff',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(${this.props.color}, ${opacity})`,
            style: {
              borderRadius: 0
              //transition: '.25s all'
              //transition-delay: '1s'
            }
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 0
            //transition: '.25s all'
            //transition-delay: '1s'
          }}
        />
      </View>
      //</SharedElement>
    );
  }
}
