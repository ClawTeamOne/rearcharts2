import React, {Component} from 'react';
import { View, Text, Dimensions } from 'react-native';
import { StackedBarChart } from 'react-native-chart-kit'
import { Svg, Circle, Line, Rect, Path, } from 'react-native-svg';

export default class StackedBarCharts extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: [0,0,0,0,0,0]
    }

  }

  render() {
    const width = (Dimensions.get('window').width - 10 )
    return (
        <StackedBarChart
          style={{
            marginVertical: 8,
            borderRadius: 0
            //transition: '.25s all'
            //transition-delay: '1s'
          }}
          data={{
            labels: this.props.labels,
            legend: this.props.legend,
            data: this.props.data,
            barColors: this.props.barColors
          }}
          width={width}
          height={this.props.height}
          yAxisLabel={'$'}
          chartConfig={{
            backgroundColor: this.props.backgroundColor || '#ffffff',
            backgroundGradientFrom: this.props.backgroundGradientFrom || '#ffffff',
            backgroundGradientTo: this.props.backgroundGradientTo || '#ffffff',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(${this.props.color}, ${opacity})`,
            style: {
              borderRadius: 0
              //transition: '.25s all'
              //transition-delay: '1s'
            }
          }}
        />
    );
  }
}
