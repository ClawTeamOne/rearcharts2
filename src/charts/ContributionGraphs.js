import React, {Component} from 'react';
import { View, Text } from 'react-native';
import { ContributionGraph } from 'react-native-chart-kit'
import { Svg, Circle, Line, Rect, Path, } from 'react-native-svg';

//import { SharedElement } from 'react-native-motion';

export default class ContributionGraphs extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: [0,0,0,0,0,0]
    }

  }

  render() {
    return (
      //<SharedElement id="source">
      <View>
        <ContributionGraph
          values={this.props.data}
          endDate={this.props.endDate}
          numDays={this.props.numDays}
          width={this.props.width} // from react-native
          height={this.props.height}
          yAxisLabel={'$'}
          chartConfig={{
            backgroundColor: this.props.backgroundColor || '#ffffff',
            backgroundGradientFrom: this.props.backgroundGradientFrom || '#ffffff',
            backgroundGradientTo: this.props.backgroundGradientTo || '#ffffff',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(${this.props.color}, ${opacity})`,
            style: {
              borderRadius: 0
              //transition: '.25s all'
              //transition-delay: '1s'
            }
          }}
          style={{
            marginVertical: 8,
            borderRadius: 0
            //transition: '.25s all'
            //transition-delay: '1s'
          }}
        />
      </View>
      //</SharedElement>
    );
  }
}
