import React, {Component} from 'react';
import Home from './pages/Home'
import Emisses from './pages/Emisses'
import Distribuction from './pages/Distribuction'
import Graphic from './pages/Graphic'

export default class Routes extends Component {

  getPage(){
    if(this.props.page === 'home')
      return (<Home />)
    else if(this.props.page === 'radio')
      return (<Emisses />)
    else if(this.props.page === 'distribution')
      return (<Distribuction />)
    else if(this.props.page === 'graphic')
      return (<Graphic />)
  }

  render() {
    return (
      this.getPage()
    );
  }
}
