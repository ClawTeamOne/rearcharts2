import React, {Component} from 'react';
import {Text, View, Dimensions} from 'react-native';

import LineCharts from '../charts/LineCharts'
import PieCharts from '../charts/PieCharts'

const labels = ['January', 'February', 'March', 'April', 'May', 'June']
const data = [
  Math.random() * 100,
  Math.random() * 100,
  Math.random() * 100,
  Math.random() * 100,
  Math.random() * 100,
  Math.random() * 100
]


const data2 = [
  { name: 'Seoul', population: 21500000, color: 'rgba(131, 167, 234, 1)', legendFontColor: '#7F7F7F', legendFontSize: 15 },
  { name: 'Toronto', population: 2800000, color: '#F00', legendFontColor: '#7F7F7F', legendFontSize: 15 },
  { name: 'Beijing', population: 527612, color: 'red', legendFontColor: '#7F7F7F', legendFontSize: 15 },
  { name: 'New York', population: 8538000, color: '#ffffff', legendFontColor: '#7F7F7F', legendFontSize: 15 },
  { name: 'Moscow', population: 11920000, color: 'rgb(0, 0, 255)', legendFontColor: '#7F7F7F', legendFontSize: 15 }
]

export default class Home extends Component {

  render() {
    const width = Dimensions.get('window').width

    return (
      <React.Fragment>
        <Text>HOME</Text>
        <LineCharts
          labels={labels}
          data={data}
          width={width}
          height={220}
          color={'0,255,0'}
        />
        <PieCharts
          data={data2}
          height={220}
          color={'235,232,71'}
        />
      </React.Fragment>
    );
  }
}
