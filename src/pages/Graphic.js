import React, {Component} from 'react';
import {Text, View, Dimensions} from 'react-native';

import ProgressCharts from '../charts/ProgressCharts'
import StackedBarCharts from '../charts/StackedBarCharts'

const data = [0.4, 0.6, 0.8]

const data2 = [
  [60, 60, 60],
  [30,30,60]
]
const labels = ['Test1', 'Test2']
const legend = ['L1', 'L2', 'L3']
const barColors = ['#dfe4ea', '#ced6e0', '#a4b0be']

export default class Graphic extends Component {

  render() {
    const width = Dimensions.get('window').width

    return (
      <React.Fragment>
        <Text>Graphic</Text>
        <ProgressCharts
          data={data}
          height={220}
          color={'235,232,71'}
        />
        <StackedBarCharts
          labels={labels}
          legend={legend}
          barColors={barColors}
          data={data2}
          height={220}
          color={'235,232,71'}
        />
      </React.Fragment>
    );
  }
}
